---
title: Bom Resolver
author: Hans Thorsen Lamm
---



| Place  | Date | Comment | Material | 
| ----------- | ------- | ------- | ------- |
| Stockholm| 2021 12 16 | Proposal to contribute Bom Resolver as open source | Suite of microservices  | 
| FossDEM 2022| 2022 02 06 | Added support to execute container as commandline utility| [FossDem 2022](https://archive.fosdem.org/2022/schedule/event/sbom_resolver_generating_detailed_sboms_for_alpine/) | 
| [Sidiatech](https://sidiatech.com) | 2023 02 09   | Workshop with a Kanban service for education developed with the resolver  | Internal | 
| [Cybernoden](https://cybernode.se) | 2023 02 28 | Activate group [Secure supply chains](https://cybernode.se/temagrupp-sakra-leveranskedjor-for-programvara-2/) | [2023-04-12](https://www.youtube.com/watch?v=b8Rr1uQR_2A) [2023-05-11](https://youtu.be/UpXK1M_iFOI)  [2023-05-26]( https://youtu.be/KX-fRT5wnTw) |
| [KTH](https://chains.proj.kth.se/) | 2023 03 08   | Presentation for research project [KTH Chains](https://www.kth.se/cdis/research/chains-1.1215337) | [Slides](Aggregator_kth_2023_02_08.pdf) [Meetup](https://www.meetup.com/kth-software-research-meetup/events/291758976/)| 
| [Cybernoden](https://cybernode.se) | 2023 04 12 | Forming meeting 1  [Secure supply chains](https://cybernode.se/temagrupp-sakra-leveranskedjor-for-programvara-2/) | [2023-04-12](https://www.youtube.com/watch?v=b8Rr1uQR_2A)  |
| [KTH](https://chains.proj.kth.se/) | 2023 04 21   | 2nd KTH Workshop on the Software Supply Chain| [Presentations](https://chains.proj.kth.se/software-supply-chain-workshop-2.html) | 
| [Cybernoden](https://cybernode.se) | 2023 05 04 | CRA/NIS2 Presentation for all members | [Recording](https://www.youtube.com/watch?v=3Yq2cCPChNY) |
| [Cybernoden](https://cybernode.se) | 2023 05 11 | Forming meeting 2  [Secure supply chains](https://cybernode.se/temagrupp-sakra-leveranskedjor-for-programvara-2/) | [2023-05-11](https://youtu.be/UpXK1M_iFOI) |
| [Cybernoden](https://cybernode.se) | 2023 05 26 | Forming meeting 3  [Secure supply chains](https://cybernode.se/temagrupp-sakra-leveranskedjor-for-programvara-2/) |   [2023-05-26]( https://youtu.be/KX-fRT5wnTw) |
| [NOSAD](https://nosad.se) | 2023 06 08 | Migrated public sector website to CDN | [Gitlab repo](https://gitlab.com/hans-lammda/nosad/-/wikis/home) |
| [Cybernode WG-4 Meeting ](https://cybernode.se) | 2023 06 14 | DDOS attack calculation on NOSAD website | [Slides](https://cybernode.se/app/uploads/2023/06/NOSAD-SBOM-cybernode_2023_06_14_public-1.pdf) [Recording](https://youtu.be/GdR4rcB7R4s)|
| [Cybernode WG-5 Meeting ](https://cybernode.se) | 2023 08 22 | Hardening of static web server  | [Slides](https://cybernode.se/app/uploads/2023/08/cybernode_2023_08_22.pdf) [Recording](https://youtu.be/y3CZFA58ybQ)|
| [Cybernoden](https://cybernode.se) | 2023 08 22  | Bomresolver subgroup  | Olle assigned to lead main group |
| [Cybernoden](https://cybernode.se)| 2023 09 12  | Knowledge sharing between [Supply chains](https://cybernode.se/working-group-secure-supply-chains-open-source) and [IoT](https://cybernode.se/event/ag-sakerhet-i-iot/)  | [Slides](2023_09_12_public.pdf) | 
| [Th1ng AB](https://th1ng.se/) | 2023 12 19   | Initial meeting development project<br><br>DDOS, IEC62433 and SBOM  | [Recording](https://zoom.us/rec/share/V6KpNZrtDX5iM0SnryxRUAexRBCiC1V9l3e1-_kA272qy2Rqatf9LKHKJGeudqRg.9-oB0JWaZwOwcmg1)<br>Password required for access  | 
| [Samnet2](https://samnet.se/) | 2024 01 18   |Cybersecurity<br> <br> Nosad and RISE| [Recording](https://play.dfri.se/w/p/nCXb5kXYWpxkgHVsBqHL6N?playlistPosition=10&resume=true)<br> 24 minutes into recording Cybernode discussed | 
| [Th1ng AB ](https://th1ng.se/) | 2024 01 30   | 2nd workshop<br><br>[Github](https://github.com/hans-lammda/msc_debian) [MSC](https://msc.io)  [Chainguard](https://chainguard.dev) | [Recording](https://zoom.us/rec/share/u4aDNsLO_wNbC32tmWWeBIJQ_Tw5Wmqc0vjHma1ekC_tETw37psdaoIcz3UTdmjd.CoPfYtJg6A9em1pA)<br><br>Password required for access<br><br>Handover and integration | 
| [KTH](https://chains.proj.kth.se/) | 2024 04 26   | Poster and Social Robot demo [KTH Chains](https://chains.proj.kth.se/software-supply-chain-workshop-3.html) | [Poster](https://chains.proj.kth.se/workshop_3_assets/posters/Secured%20(%20defined%20)%20or%20Compliant%20(%20declared%20)%20SBOM.pdf)  [Full Metal Scrum](https://www.youtube.com/watch?v=N4Zp1r1q3HI) | 








