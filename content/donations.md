---
title: Bom Resolver
author: Hans Thorsen Lamm
---



This project is about compliance with emerging requirements from EU and US. With automation evidence could be generated with high degree 

of automation. Developers could focus on functionality, but it comes with a cost. 

- Development 
- Hosting 
- Documentation 



The current status of the project works for Alpine 3.16 and binary images. For the limited references suite most of the packages could be recreated from source. With additional funding and external contributions I hope that all packages could be handled. 

The resolver model could also be applied to other Linux vendors and also Python , Node and Golang. 






| Contributor | Comment | 
| ----------- | ------- |
| [Sidiatech](https://sidiatech.com) | Contribution of 50.000 SEK for development and hosting  |  



Hans Thorsen Lamm 

Lamm Consulting AB 



