.PHONY: static


# Develop content 
dev: 
	(  hugo server -D  --port $(PORT)  --bind="0.0.0.0" --baseURL=$(SERVER_NAME) ) 

# Generate static content 
static: 
	(  hugo  ) 


PORT=8080
SRV_ROOT=$(PWD)/public
SERVER_NAME="http://localhost:$(PORT)/hugo"

hugo:
	hugo server  -verbose --config config.toml  --port $(PORT)  --bind="0.0.0.0" --baseURL=$(SERVER_NAME)  public 

light: static
	export SERVER_ROOT=$(SRV_ROOT)  && export PORT=$(PORT) && export SERVER_NAME=$(SERVER_NAME) && export MAX_FDS=16384 && /usr/sbin/lighttpd  -D -f  lighttpd.conf


# Remove static content 
clean: 
	rm -rf public 


#
# Container 
#

BASE_OS_IMAGE=localhost/alpine_base_lighttpd:1.0.1
SERVICE_IMAGE =lighttpd_hugo_alpine:1.0.1

build: 
	podman build -t  $(SERVICE_IMAGE) --build-arg BASE_OS_IMAGE=$(BASE_OS_IMAGE)  -f Containerfile .

OPTIONS = -p  $(PORT):$(PORT)
OPTIONS += -e PORT=$(PORT)
OPTIONS += -e SERVER_ROOT=/hugo
OPTIONS += -e USER=lighttpd
OPTIONS += -e SERVER_NAME="http://localhost:8080/hugo"
OPTIONS += -e MAX_FDS=16384

run:
	podman run -i -t  $(OPTIONS)  $(SERVICE_IMAGE) server


ff: 
	firefox $(SERVER_NAME) 

REGISTRY ?= docker.io
REGISTRY = registry.digitalocean.com
REPOSITORY ?= bomres
REPOSITORY = lammda

tag:
	podman tag $(SERVICE_IMAGE) $(REGISTRY)/$(REPOSITORY)/$(SERVICE_IMAGE)
login:
	podman login $(REGISTRY)
push:
	podman push $(REGISTRY)/$(REPOSITORY)/$(SERVICE_IMAGE)


